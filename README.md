# Codemapp

Codemapp is a code mini map generator (similar to [code-minimap](https://github.com/wfxr/code-minimap))

## Compile and build

```bash
$ meson setup releasedir --buildtype release
$ meson compile -C releasedir
```

## Usage

From the build folder:

```bash
$ ./releasedir/codemapp any-file.js`
```

Or from the stdin:

```bash
$ echo -e "hello world\nbye cruel world" | ./releasedir/codemapp
```

## Installation

It has no installation methods yet, but since it only uses the standard library you can just move the binary to your system `bin` directory.

## Examples

- Code mini map from full source:

```bash
$ ./builddir/codemapp src/main.cpp

⣿⣿⣿⣿⢸⣿⣿⣿⠿⠥⠄
⣛⣛⣛⡛⣘⣛⣛⣛⣋⣉⢀⣀⣀⣀⣀⡀
⠒⣶⣶⣶⢢⣶⣶⡖⣶⣶⣶⢲⣶⣶⡖⣶⣶⣶⢲⣴⣶⡆⣶⣶⣶⢲⣶⣶⡔⣴⣤⣤⢠⣤⣤⡄⣤⣤⣤⢠⣤⣤⡄⣤⣤⣤⢠⣤⣤⡄⣤⣤⣤
⠀⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿
⠀⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿
⠀⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿⢸⣿⣿⡇⣿⣿⣿
⠀⣿⣿⡿⠸⠿⠿⠇⠿⠿⠿⠸⠿⠿⠇⠿⠿⠿⠸⠿⠿⠇⠿⠿⠿⠸⠿⠿⠇⠿⠿⠿⠸⠿⠿⠇⠿⠿⠿⠸⠿⠿⠇⠿⠿⠿⠸⠿⠿⠇⠿⠿⠿
⠭⣤⣤⣤⣤⣤⣤⣤⣠⢤⣤⣤⡤⠤⠤⠤⠤
⡀⡿⠿⠿⠿⠿⠿⠿⠿⠸⠿⠿⠃
⠀⣭⣭⣭⣭⣭⣭⢩⣬⣭⣭⣥⣤⢠⡄⡄⣤⡄
⠀⣛⣛⣛⣛⣛⣛⢘⣛⣛⣛⣛⣛⡊⣓⢑⢙⢑⣒⣒⢐⢐⣒⣒⣒⣒⣒⢐⡂⡂⣒⡂
⠀⠿⠿⠿⠿⠿⠿⠸⠿⠿⠿⠿⠿⠕⠯⠪⠺⠪⠭⠭⠨⠨⠭⠭⠭⠭⠭⠨⠅⠅⠭⠅
⠂⠉⠉⠉⠈⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠈⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠉⠁
⠉⣿⣾⣿⣿⣿⣿⣿⣷⢰⣶⣶⡆
⠂⠯⡭⠩⠭⠭⠭⠩⠩⠬⠭⠭⠄⠤⠤⠤⠤⠤⠤⠤⠤⠠⠤⠠⠤⠤⠠⠤⠤⠤⠤⠤⠤⠤⠄⠤⠄
⠀⣀⣒⢉⣉⣉⣈⣉⣉⣉⡉⡈⣈⣉⣉⣉⣉⣁⣀⣀⢀⣀⣀⡀⣀⣀⣀⢀⣀⣀⣀⢀⣀⣀⣀⣀
⠀⠒⠶⣶⣶⣶⣖⣴⣒⣐⣐⡐⠂⠂⠂⠒⠒⠒⠒⠒⠒⠒⠒⠒⠒⠒⠐⠐⠐⠐⠐⠂⠂
⠀⠀⠀⠀⡇⡇⣿⣿⣿⣿⣿⣿⢸⢸⣿⣿⣿⣿⣿⣿⡇⡇⣿⣿⣿⣿⣿⣿⡇⣿⢸⢸⠀⡿
⠀⣂⣀⣉⣀⡀⣀⢀⣀⣀
⠥⣤⣠⣤⣤⣤⣤⣤⣄⢀⣀⣀⡀
⠄⣟⣛⣛⣛⣛⣛⣛⣛⣘⢛⣛⣁⡀⣀⣀⣀⢀⠀⣀⡀
⠥⣤⣠⣤⣤⣤⣤⣤⣄⢀⣀⣀⡀
⡂⠯⠭⠭⠭⠭⠭⠭⠭⠬⠩⠭⠤⠄⠄⠠⠤
⡒⡶⠴⠶⠶⠶⠶⠶⠦⠠⠤⠤
⣂⡉⣉⣉⣉⣉⢉⣉⣉⢉⣈⡀⣉⣁⣀⡀⡀
⠀⡉⠾⠿⠿⠺⠇⠛⠘⠒⠒⠒⠐⠂⠐⠒⠒⠂⠒⠒⠐⠒⠐⠒⠒⠒⠒⠒⠒
⠀⣒⢒⣒⣒⣒⣒⣒⣒⣒⣒⣂⣒⣒⣐⣐⣒⣒⣒⣂⣀⣀⣀⢀
⠀⠄⠛⠛⠛⠙⠃⠉⠈⠉⠉⠉⠈⠁⠁⠉⠈⠉⠈⠈⠉⠉⠉⠈⠉⠉⠈⠁⠉⠉⠉⠉⠉
⠀⠭⣩⣭⣭⣭⡭⣬⢭⢭⣭⣭⣭⡭⣍⣍⢁⣀⣀⢀⢀⡀⣀⣀⣀⡀⣀⢀⣀⣀⣀⣀⡀
⠀⣂⣉⣉⣉⣈⣁⣀⣀⣀⣀⣀⡀⣀⢀⣀⣀⣀⣀⣀⡀
⠀⠿⣯⣞⣾⣿⣿⣿⡼⠽⠯⠥⠭⠭⠭⠬⠭⠩⠥⠥⠭⠭⠭⠭⠭⠭⠤⠤⠤⠤⠤⠄⠄
⠀⠀⠒⣴⣶⣶⣶⣶⢶⢴⡦⠶⠰⠤⠤⠄⠤⠤⠤⠠⠤⠤⠤
⠀⣂⣁⣀⣀⣀⣀⣀⣀⣀⢀
⠀⠀⣉⣿⢾⡷⠒⠒⠒⠒⠒⠒⠂
⠀⠀⠤⣿⣻⣟⣉⣉⣉⣉⣉⣉⢈⣉⣉⡉⣀⣀⣀⡀
⡀⠥⠤⠤⠠⠄
```

- Reading from the stdin:

```bash
$ echo -e "hello world\nbye cruel world" | ./releasedir/codemapp

⠛⠋⠓⠛⠋⠓⠒⠂
```

## Benchmark and comparison with code-minimap

Even though performance is not the top priority, in benchmarks, codemapp had a very satisfactory result with a huge file.

```bash
$ base64 /dev/urandom | head -10000000 > huge.txt
$ hyperfine --warmup 10 './releasedir/codemapp huge.txt' 'code-minimap huge.txt'

Benchmark 1: ./releasedir/codemapp huge.txt
  Time (mean ± σ):      1.865 s ±  0.035 s    [User: 1.779 s, System: 0.080 s]
  Range (min … max):    1.836 s …  1.939 s    10 runs
 
Benchmark 2: code-minimap huge.txt
  Time (mean ± σ):      2.960 s ±  0.046 s    [User: 2.529 s, System: 0.424 s]
  Range (min … max):    2.911 s …  3.054 s    10 runs
 
Summary
  ./releasedir/codemapp huge.txt ran
    1.59 ± 0.04 times faster than code-minimap huge.txt
```
