#include <iostream>
#include <fstream>
#include <filesystem>
#include <array>
#include <algorithm>
#include <bitset>
#include <istream>

#define PROJECT_NAME "codemapp"

constexpr std::array<std::string_view, 256> braille_utf8 = {
  "⠀", "⠁", "⠂", "⠃", "⠄", "⠅", "⠆", "⠇", "⠈", "⠉", "⠊", "⠋", "⠌", "⠍", "⠎",
  "⠏", "⠐", "⠑", "⠒", "⠓", "⠔", "⠕", "⠖", "⠗", "⠘", "⠙", "⠚", "⠛", "⠜", "⠝",
  "⠞", "⠟", "⠠", "⠡", "⠢", "⠣", "⠤", "⠥", "⠦", "⠧", "⠨", "⠩", "⠪", "⠫", "⠬",
  "⠭", "⠮", "⠯", "⠰", "⠱", "⠲", "⠳", "⠴", "⠵", "⠶", "⠷", "⠸", "⠹", "⠺", "⠻",
  "⠼", "⠽", "⠾", "⠿", "⡀", "⡁", "⡂", "⡃", "⡄", "⡅", "⡆", "⡇", "⡈", "⡉", "⡊",
  "⡋", "⡌", "⡍", "⡎", "⡏", "⡐", "⡑", "⡒", "⡓", "⡔", "⡕", "⡖", "⡗", "⡘", "⡙",
  "⡚", "⡛", "⡜", "⡝", "⡞", "⡟", "⡠", "⡡", "⡢", "⡣", "⡤", "⡥", "⡦", "⡧", "⡨",
  "⡩", "⡪", "⡫", "⡬", "⡭", "⡮", "⡯", "⡰", "⡱", "⡲", "⡳", "⡴", "⡵", "⡶", "⡷",
  "⡸", "⡹", "⡺", "⡻", "⡼", "⡽", "⡾", "⡿", "⢀", "⢁", "⢂", "⢃", "⢄", "⢅", "⢆",
  "⢇", "⢈", "⢉", "⢊", "⢋", "⢌", "⢍", "⢎", "⢏", "⢐", "⢑", "⢒", "⢓", "⢔", "⢕",
  "⢖", "⢗", "⢘", "⢙", "⢚", "⢛", "⢜", "⢝", "⢞", "⢟", "⢠", "⢡", "⢢", "⢣", "⢤",
  "⢥", "⢦", "⢧", "⢨", "⢩", "⢪", "⢫", "⢬", "⢭", "⢮", "⢯", "⢰", "⢱", "⢲", "⢳",
  "⢴", "⢵", "⢶", "⢷", "⢸", "⢹", "⢺", "⢻", "⢼", "⢽", "⢾", "⢿", "⣀", "⣁", "⣂",
  "⣃", "⣄", "⣅", "⣆", "⣇", "⣈", "⣉", "⣊", "⣋", "⣌", "⣍", "⣎", "⣏", "⣐", "⣑",
  "⣒", "⣓", "⣔", "⣕", "⣖", "⣗", "⣘", "⣙", "⣚", "⣛", "⣜", "⣝", "⣞", "⣟", "⣠",
  "⣡", "⣢", "⣣", "⣤", "⣥", "⣦", "⣧", "⣨", "⣩", "⣪", "⣫", "⣬", "⣭", "⣮", "⣯",
  "⣰", "⣱", "⣲", "⣳", "⣴", "⣵", "⣶", "⣷", "⣸", "⣹", "⣺", "⣻", "⣼", "⣽", "⣾",
  "⣿"
};

std::string_view get_minimap_hint(
  std::string_view line1,
  std::string_view line2,
  std::string_view line3,
  std::string_view line4
) {
  std::bitset<8> index;

  index.set(0, line1.at(0) != ' ');
  index.set(1, line2.at(0) != ' ');
  index.set(2, line3.at(0) != ' ');
  index.set(3, line1.size() == 1 ? false : line1.at(1) != ' ');

  index.set(4, line2.size() == 1 ? false : line2.at(1) != ' ');
  index.set(5, line3.size() == 1 ? false : line3.at(1) != ' ');
  index.set(6, line4.at(0) != ' ');
  index.set(7, line4.size() == 1 ? false : line4.at(1) != ' ');

  return braille_utf8[static_cast<unsigned int>(index.to_ulong())];
}


void print_lines(
  std::string_view line1,
  std::string_view line2,
  std::string_view line3,
  std::string_view line4
) {
  auto longest = [](const std::string_view s1, const std::string_view s2)
    {
      return s1.size() < s2.size();
    };

  auto longest_string = std::max({line1, line2, line3, line4}, longest);

  for(unsigned int i = 0; i < longest_string.size(); i = i + 2) {
    std::cout <<
      get_minimap_hint(
        i < line1.size() ? line1.substr(i % line1.size(), 2) : "  ",
        i < line2.size() ? line2.substr(i % line2.size(), 2) : "  ",
        i < line3.size() ? line3.substr(i % line3.size(), 2) : "  ",
        i < line4.size() ? line4.substr(i % line4.size(), 2) : "  "
      );
  }

  std::cout << '\n';
}

void print_lines(
  std::string_view line1,
  std::string_view line2,
  std::string_view line3
) {
  print_lines(line1, line2, line3, "  ");
}

void print_lines(
  std::string_view line1,
  std::string_view line2
) {
  print_lines(line1, line2, "  ");
}

void print_lines(
  std::string_view line1
) {
  print_lines(line1, "  ");
}

int main(int argc, char **argv) {

  std::unique_ptr<std::istream> stream;

  if (argc != 2) {
    stream = std::unique_ptr<std::istream>(new std::istream(std::cin.rdbuf()));
  } else {
    std::filesystem::path file = argv[1];

    if (!std::filesystem::is_regular_file(file)) {
      std::cout << argv[1] << " is not a regular file" << std::endl;
      return 2;
    }

    std::unique_ptr<std::fstream> file_stream(new std::fstream(file));

    if (!file_stream->is_open()) {
      std::cout << " couldn't open file: " << argv[1] << std::endl;
      return 3;
    }

    stream = std::move(file_stream);
  }

  std::array<std::string, 4> input_lines;
  auto & [line1, line2, line3, line4] = input_lines;
  int line_count = 0;
  while (std::getline(*stream, input_lines.at(line_count))) {
    line_count++;

    if (line_count == 4) {
      print_lines(line1, line2, line3, line4);
      line_count = 0;
    }
  }

  switch(line_count) {
    case 1:
      print_lines(line1);
      break;
    case 2:
      print_lines(line1, line2);
      break;
    case 3:
      print_lines(line1, line2, line3);
  }

  return 0;
}
